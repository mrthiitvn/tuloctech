﻿$(function () {
    //slider
    $('.slick-single').slick({
        arrows: false,
        dots: true,
        autoplay: true,
        fade: false
    });

    //tooltip
    $('.tooltip').tooltipster({
        theme: 'tooltipster-borderless',
        //side: ['right', 'left', 'top', 'bottom'],
        plugins: ['tooltipster.sideTip', 'laa.scrollableTip'],
        'laa.follower': {
            anchor: 'right-center'
        },
        repositionOnScroll: true,
        contentAsHTML: true,
		interactive: true,
        maxWidth: 400,
        side: 'right'
    });

    //tạo kiểu cho số lượt truy cập
    var numbers = $('#visit-counter-total').text().split("");
    console.log(numbers);
    $('#visit-counter-total').empty();
    for (var i = 0; i < numbers.length; i++) {
        $('#visit-counter-total').append("<span>" + numbers[i] + "</span>");
    }

    $('#image-gallery').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        thumbItem: 6,
        slideMargin: 0,
        enableDrag: false,
        currentPagerPosition: 'left',
        adaptiveHeight: true
    });

    $(".menu-toggler").click(function () {
        $("#menubar").toggleClass("show");
    });
});
